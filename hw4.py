import itertools
import os
import community as com
from string import punctuation
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt
import csv
import plotly.plotly as py
import plotly.figure_factory as ff

import numpy as np
from scipy.cluster.hierarchy import dendrogram


graph = nx.Graph()
graphx = nx.Graph()
# open csv file
with open('casts-2.csv', 'r') as csvfile:
    spamreader2 = csv.reader(csvfile, delimiter=';')
    included_cols = [1, 2]
    
    films2={}
    film2=[]
    actor2=[]
    for row in spamreader2:
        actor2.append(row[2])
        film2.append(row[1])
        graphx.add_node(row[2])   
    
    for i in range(1,len(film2)):
      
        if film2[i] == film2[i-1]:
            if actor2[i] != actor2[i-1]:
                graphx.add_edge(actor2[i],actor2[i-1])

with open('casts.csv', 'r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=';')
    included_cols = [1, 2]
    
    films={}
    film=[]
    actor=[]
    for row in spamreader:
        actor.append(row[2])
        film.append(row[1])
        graph.add_node(row[2])   
    
    for i in range(1,len(film)):
      
        if film[i] == film[i-1]:
            if actor[i] != actor[i-1]:
                graph.add_edge(actor[i],actor[i-1])
            
density=(len(graph.nodes())*(len(graph.nodes())-1))/2


components=nx.number_connected_components(graph)
 
print("Number of nodes: ", len(graph.nodes()))
print("Number of edges: ",len(graph.edges()))
print("Density: ",density)
print("NUmber of components:", components)



pos = graphviz_layout(graphx, prog="fdp")
centralities = [nx.degree_centrality, nx.closeness_centrality,
nx.betweenness_centrality]

for centrality in centralities:
    graphc = centrality(graphx)
    centrality_sorted = sorted(graphc.items(), key=lambda element: element[1], reverse=True)
    print(centrality_res_sorted[:10])

nx.write_gexf(graphx, "export-hw4.gexf")